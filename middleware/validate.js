var dontCheckTheseKeys = ['_local', 'latLng'];

var validate = function(req, res, next) {
  for(var key in req.body) {
    if(req.body.hasOwnProperty(key)) {
      if(req.body[key] == '') {
        if(dontCheckTheseKeys.indexOf(key) <= -1) {
          res.redirect(req.get('referer'));
          console.log("someone left out a field\n");
          console.log(req.body);
          return
        }
      }
    }
  }
  next();
}
module.exports = validate;
