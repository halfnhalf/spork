var SporkAPI = function() {};
var Yelp = require('yelp');
//var Event = require('../js/server/models');
var flatfile = require('flat-file-db');
var db = flatfile.sync('./spork.db');

var debug = true;

var getIpFromRequest = function(req) {
  return req.headers['x-forward-for'] || req.connection.remoteAddress;
  //console.log(req);
  //return req.ip
}

var sortSuggestions = function(event) {
  event.suggestions.sort(function(a, b) {return b.sporkVotes - a.sporkVotes});
}

var makeId = function() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for(var i = 0; i < 5; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

var didUserVote = function(req, event) {
  if(event.usersWhoVoted.indexOf(getIpFromRequest(req)) > -1)
    return true;
  return false;
}

SporkAPI.yelp = new Yelp({
  consumer_key: 'frJS-Ywd4qBP14X_V29MAQ',
  consumer_secret: 'E2OF4pnoiveFvyJOQWM5V8Xn5qA',
  token: 'oL9QFntMZ7579H9Qqb6JL6_QV8kylF9V',
  token_secret: 'rMJGLPH8lUn4JedOybwqmQWKHoM'
});

SporkAPI.constants = {};
//number of responses
SporkAPI.constants.limit = 3

SporkAPI.prototype.queryYelp = function(req, res, next) {
  try {
    var term = req.query.term;
    var location = req.query.location;

    if(typeof term === 'undefined' || typeof location === 'undefined')
      throw "undefined param" ;

    console.log("YELPAPI request from "+ location);

    SporkAPI.yelp.search({term: term, location: location, limit: SporkAPI.constants.limit})
      .then(function(data) {
        //console.log(data);
        res.json(data);
        next();
      })
      .catch(function(err) {
        console.log(error);
        next();
      });
  } catch(err) {
    console.log(err);
    res.redirect('/');
    next();
  }
}

SporkAPI.prototype.vote = function(req, res, next) {
  try{
    var voteId = req.body.vote;
    var id = req.params.id;
    var event = db.get(id);

    if(didUserVote(req, event)) {
      throw new Error("alreadyVoted");
    }

    event.suggestions.forEach(function(business) {
      if(business.id === voteId) {
        var ip = getIpFromRequest(req);
        business.sporkVotes = business.sporkVotes + 1;
        event.usersWhoVoted.push(ip); 
        res.cookie('didVote', true, {path:'/event/'+id});
        sortSuggestions(event);
        console.log(ip+" voted for "+voteId+" from event " + id);
      }
    });

    db.put(req.params.id, event);
    next();
  } catch(err) {
    if(err.message === "alreadyVoted") {
      res.send('You already voted.');
      return;
    }
    console.log(err);
    next();
  }
}

SporkAPI.prototype.getEvent = function(req, res, next) {
  try {
    var id = req.params.id;
    if(!db.has(id))
      throw new Error("invalidId");
    var info = db.get(req.params.id);
    info.eventId = req.params.id;
    info.didUserVote = didUserVote(req, info);

    if(debug) {
      console.log("GET EVENT:\n");
      console.log(info);
    }

    res.eventInfo = info;
    next();
  } catch(err) {
    if(err.message === "invalidId") {
      res.send("No event found");
      return;
    }
    console.log(err);
    next();
  }
}

SporkAPI.prototype.createEvent = function(req, res, next) {
  try {
    var id = makeId();
    while(db.has(id))
      id = makeId();

    req.body.suggestions = JSON.parse(req.body.suggestions);
    req.body.suggestions.forEach(function(business) {
      business.image_url = business.image_url.replace("ms.jpg", "o.jpg");
      business.sporkVotes = 0;
    });
    req.body.usersWhoVoted = [];
    console.log("New event created: "+id);
    db.put(id, req.body);
    res.redirect('/event/'+id);
    /*var newEvent = Event({
      signin: req.body.signin,
      location: req.body.location,
      date: req.body.date,
      time-hour: req.body.time-hour,
      time-minute: req.body.time-minute,
      ampm: req.body.ampm
    });*/

    next();

  } catch(err) {
    console.log(err);
    next();
  }
}

module.exports = new SporkAPI;
