function initMap() {
  var mapOptions = {
    zoom: 16,
    streetViewControl: false,
    scaleControl: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    center: {lat: -34.397, lng: 150.644}
  };
  map = new google.maps.Map(document.getElementById('googlemaps'), mapOptions);

  if(navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {

      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      var latLng = new google.maps.LatLng(pos.lat, pos.lng);

      geocoder.geocode({latLng: latLng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {

          if (results[1]) {
            var arrAddress = results;
            console.log(results);
            $.each(arrAddress, function(i, address_component) {

              if (address_component.types[0] == "street_address") {
                address = address_component.formatted_address;

                $("#location :input[name='location']").val(address);
                $("#location :input[name='latLng']").val(JSON.stringify(pos));
              }
            });
          } else {
            alert("No results found");
          }
        } else {
          alert("Geocoder failed due to: " + status);
        }
      });
      map.setCenter(pos);
      var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        draggable: false,
        animation: google.maps.Animation.DROP
      });

    });
  }
}
geocoder = new google.maps.Geocoder();
google.maps.event.addDomListener(window, 'load', initMap);

//check if the user entered an address. if they did get the lat lng
//var form = document.getElementById('location')
//
//form.addEventListener('submit', function() {
//  var latLngInput = $("#location :input[name='latLng']");
//  if(!latLngInput.val()) {
//    var address = document.getElementsByName("location")[0].value;
//    geocoder.geocode({'address': address}, function(results, status) {
//      if(status == google.maps.GeocoderStatus.OK) {
//        var pos = {
//          lat: position.coords.latitude,
//          lng: position.coords.longitude
//        };
//
//        latLngInput.val(JSON.stringify(pos));
//      }
//    });
//  } else {
//    return
//  }
//  form.submit();
//}, false);
