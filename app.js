var express = require('express');
var app = express();

//express settings
app.set('view engine', 'jade');
//app.set('trust proxy');

//load middleware
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use(express.static('public'));
app.use(require('./controllers'));
//use 8082 for deployment
app.listen(3000, function() {
  console.log('Running');
})
