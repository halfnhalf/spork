var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var validate = require('../middleware/validate');
var sporkAPI = require('../middleware/sporkAPI');

//middleware
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));


router.post('/location', validate, function(req, res) {
  res.render('location', req.body);
});

router.post('/timedate', validate, function(req, res) {
	res.render('timedate', req.body);
  //console.log(req.body);
});

router.post('/place', validate, function(req, res) {
  res.render('place', req.body);
  //console.log(req.body);
});

router.post('/event', sporkAPI.createEvent, function(req, res) {
  //console.log(req.body.suggestions);
});

router.post('/event/:id/vote', sporkAPI.vote, function(req, res) {
  res.redirect('/event/' + req.params.id);
});

router.get('/event/:id', sporkAPI.getEvent, function(req, res) {
  res.render('results', res.eventInfo);
});


router.get('/api/yelp', sporkAPI.queryYelp, function(req, res) {
  res.end();
});

/*
router.get('/share', function(req, res) {
  res.render('share');
});

router.get('/invite', function(req, res) {
  res.render('invite1');
});


router.get('/invite-confirm', function(req, res) {
  res.render('invite2');
});

router.get('/results', function(req, res) {
  res.render('results');
});
*/

router.get ('/', function(req, res) {
  res.render('index');
});


router.get('*', function(req, res) {
  res.redirect('/');
});

module.exports = router;
