module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      dist: {
        files: {
          'public/stylesheets/style.css' : 'scss/app.scss'
        }
      }
    },
    watch: {
      css: {
        files: '**/*.scss',
        tasks: ['sass']
      },
      js: {
        files: 'js/**/*.js',
        tasks: ['browserify']
      }
    },
    browserify: {
      dist: {
        options: {
          transform: [
            ['babelify', {presets: 'react'}]
          ]
        },
        src: ['js/client/*.js', 'bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js', 'bower_components/bootstrap-sass/assets/javascripts/bootstrap/transition.js' ],
        dest: 'public/js/bundle.js'
      }
    }
  });
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default', ['watch']);
  grunt.registerTask('build', ['sass', 'browserify']);
}
