var React = require('react')
var ReactDOM = require('react-dom');

var Suggestions = React.createClass({
  removeSuggestion: function(business) {
    this.props.removeSuggestion(business);
  },

  render: function() {
    var rows=[];
    var suggestions = this.props.suggestions;

    try {
      rows = suggestions.map(function(business) {

        var backgroundStyle = {
          background: 'linear-gradient(rgba(0,0,0,.75), rgba(0,0,0,0.3)), url('+business.image_url.replace("ms.jpg", "o.jpg")+')',
          backgroundPosition: 'center'
        };

        var inputTitle = 'Suggest '+ business.name +' to your friends';
        var toggleHref = '#'+business.id+'collapse';
        var toggleControls = business.id+'collapse';
        var yelpStarImage = String(business.rating_img_url_large);
        var collapseId = business.id+'collapse';
        var numReviews = String(business.review_count);
        var categories = String(business.categories);
        var googleMapsLink = 'http://maps.google.com/?q=' + business.name + ' ' + business.location.address;

        return(
          <li>
            <ul className="yelp-result" style={backgroundStyle}>
              <li className="placename">
              	<a role="button" data-toggle="collapse" href={toggleHref} aria-expanded='false' aria-controls={toggleControls}>{business.name}</a>
              	<button type="button" className="btn btn-success remove" onClick={this.removeSuggestion.bind(this, business)}>Remove</button>
              </li>
              <li className="placereview">
                <img className="" src={yelpStarImage}></img>
              </li>
              <li className="moreinfo">
                <div className="collapse" id={collapseId}>
                  <div className="well">
                    <ul>
                      <li className="price"></li>
                      <li className="category">{categories}</li>
                      <li className="address">
                        <a href={googleMapsLink}>
                          {business.location.address}
                          <br></br>
                          {business.location.city}, {business.location.state_code} {business.location.postal_code}
                        </a>
                      </li>
                      <li className="phone">
                        <a href={business.phone}>{business.display_phone}</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
            </ul>
          </li>
        );
      }.bind(this));

      if(rows[0] == null){
        //return <div>No results found</div>
      }

    } catch(err) {
      console.log(err);
      //return <div>Loading...</div>
    }
    return (
      <ul className="yelp-results">{rows}
        <input type="hidden" name="suggestions" value={JSON.stringify(suggestions)} required></input>
      </ul>
    );
  }
});

var YelpResults = React.createClass({
  addSuggestion: function(business) {
    this.props.addSuggestion(business);
    //console.log(business);
  },

  render: function() {
    /*var rows=[];
    this.props.data.forEach(function(result) {
      rows.push(<li>{result}</li>);
    });*/
    var rows=[];
    var yelpData = this.props.data;

    try {
      rows = yelpData.businesses.map(function(business) {

        var backgroundStyle = {
          background: 'linear-gradient(rgba(0,0,0,.75), rgba(0,0,0,0.3)), url('+business.image_url.replace("ms.jpg", "o.jpg")+')',
          backgroundPosition: 'center'
        };

        var inputTitle = 'Suggest '+ business.name +' to your friends';
        var toggleHref = '#'+business.id+'collapse';
        var toggleControls = business.id+'collapse';
        var yelpStarImage = String(business.rating_img_url_large);
        var collapseId = business.id+'collapse';
        var numReviews = String(business.review_count);
        var categories = String(business.categories);
        var googleMapsLink = 'http://maps.google.com/?q=' + business.name + ' ' + business.location.address;

        return(
          <li>
            <ul className="yelp-result" style={backgroundStyle}>
              <li className="placename">
              	<a role="button" data-toggle="collapse" href={toggleHref} aria-expanded='false' aria-controls={toggleControls}>{business.name}</a>
              	<button type="button" className="btn btn-success suggest" onClick={this.addSuggestion.bind(this, business)}>Suggest</button>
              </li>
              <li className="placereview">
                <img className="" src={yelpStarImage}></img>
              </li>
              <li className="moreinfo">
                <div className="collapse" id={collapseId}>
                  <div className="well">
                    <ul>
                      <li className="price"></li>
                      <li className="category">{categories}</li>
                      <li className="address">
                        <a href={googleMapsLink}>
                          {business.location.address}
                          <br></br>
                          {business.location.city}, {business.location.state_code} {business.location.postal_code}
                        </a>
                      </li>
                      <li className="phone">
                        <a href={business.phone}>{business.display_phone}</a>
                      </li>
                      <li className="yelplink">
                        <a href={business.url} target="_blank">View more on
                          <img src="/img/yelp-logo-medium@2x.png"></img>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
            </ul>
          </li>
        );
      }.bind(this));

      if(rows[0] == null){
        return <div>No results found</div>
      }

    } catch(err) {
      console.log(err);
      return <div>Loading...</div>
    }
    return <ul className="yelp-results">{rows}</ul>
  }
});

var InstantSearch = React.createClass({
  componentDidUpdate: function() {
    //this.refs.placeInput.scrollTop = 0;
  },

  doSearch: function(queryText) {
    console.log(queryText);

    //check if the new query is a substring of the old one
    this.serverRequest = $.getJSON('/api/yelp', {
      term: queryText,
      location: this.props.location
    })
    .done(function(data) {
      this.setState({
        yelpData: data,
        query: queryText
      })
    }.bind(this));
  },

  getDefaultProps: function() {
    var location = $("input[name='location']").val();

    return {
      location: location.split(' ').join('+')
    };
  },

  getInitialState: function() {
    return {
      yelpData: {businesses:[]},
      query: '',
      suggestions: []
    };
  },

  componentDidMount: function() {
    this.serverRequest = $.getJSON('/api/yelp', {
      term: 'food',
      location: this.props.location
    })
    .done(function(data) {
      //console.log(data);
      this.setState({
        yelpData: data
      });
    }.bind(this));
  },
  
  addSuggestion: function(business) {
    this.setState({
      suggestions: this.state.suggestions.concat([business])
    });
  },

  removeSuggestion: function(business) {
    var index = this.state.suggestions.indexOf(business);

    if(index > -1) {
      var newList = this.state.suggestions;
      newList.splice(index, 1);
      this.setState({
        suggestions: newList
      });
    }
  },

  render: function() {
    return (
      <div>
        <h3 className="step-header">What are you in the mood for?</h3>
        <div className="form-container form-group">
        <p className="instructions">Pick some suggestions for attendees to vote on</p>
        <PlaceInput ref="placeInput" doSearch={this.doSearch} />
        </div>
        <YelpResults data={this.state.yelpData} query={this.state.query} addSuggestion={this.addSuggestion}/>
        <h3 className="step-header">Places you want to suggest appear under here</h3>
        <Suggestions suggestions={this.state.suggestions} removeSuggestion={this.removeSuggestion}/>
        <a id='powered-by-yelp' className='center-block' href='http://yelp.com' target='_blank'>
          <img src="/img/yelp_powered_btn_light@2x.png" alt="Powered by Yelp" width="129" height="30"></img>
        </a>
      </div>
    );
  }
});

var PlaceInput = React.createClass({
  doSearch: function() {
    var query = this.refs.placeInput.value;
    this.props.doSearch(query);
  },
  render: function() {
    return <input ref="placeInput" id="placeInput" className="text-input form-control input-lg" type="text" name="place" autofocus placeholder='Enter something like "Italian" or "Ice Cream."' onChange={this.doSearch}></input>
  },
});

ReactDOM.render(<InstantSearch />, document.getElementById("instantSearch"))
