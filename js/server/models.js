var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var eventSchema = new Schema({
  id: String,
  signin: String,
  location: String,
  date: String,
  time-hour: Number,
  time-minute: Number,
  ampm: String
});

var Event = mongoose.model('Event', eventSchema);

model.exports = Event;
